package blockchain

import (
	"bytes"
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
	"umi-master/global"
	"umi-master/storage/pg"
)

var httpClient = &http.Client{
	Timeout: time.Second * 5,
}

func EventLoop(ctx context.Context) error {
	miner := time.NewTicker(3 * time.Second)

	for {
		select {
		case <-miner.C:
			GenerateBlock(ctx)
		case <-ctx.Done():
			miner.Stop()
			return nil
		}
	}
}

type prm struct {
	Height uint64 `json:"height,omitempty"`
	Base64 string `json:"base64,omitempty"`
}

type req struct {
	Jsonrpc string `json:"jsonrpc"`
	Method  string `json:"method"`
	Params  prm    `json:"params"`
	Id      string `json:"id"`
}

type rsp struct {
	Result []string `json:"result"`
}

func Puller(ctx context.Context) error {
	ticker := time.NewTicker(5 * time.Second)

	for {
		select {
		case <-ticker.C:
			var lstBlkHeight uint64
			if err := pg.Db.QueryRow(ctx, "select max(height) from block").Scan(&lstBlkHeight); err != nil {
				continue
			}

			rq, _ := json.Marshal(req{
				Jsonrpc: "2.0",
				Method:  "listBlocks",
				Params:  prm{Height: lstBlkHeight + 1},
				Id:      fmt.Sprintf("%d", time.Now().Nanosecond()),
			})

			url := "https://mainnet.umi.top/json-rpc"
			if global.TestNet {
				url = "https://testnet.umi.top/json-rpc"
			}

			res, err := httpClient.Post(url, "application/json", bytes.NewBuffer(rq))
			if err != nil {
				continue
			}

			dat, err := ioutil.ReadAll(res.Body)
			res.Body.Close()

			if err != nil {
				continue
			}

			resp := &rsp{}
			err = json.Unmarshal(dat, resp)

			if err != nil {
				continue
			}

			for _, b := range resp.Result {
				z, err := base64.StdEncoding.DecodeString(b)
				if err != nil {
					continue
				}

				if _, err := pg.Db.Exec(ctx, "select add_block($1)", z); err != nil {
					continue
				}
			}
		case <-ctx.Done():
			ticker.Stop()
			return nil
		}
	}
}

func Pusher(ctx context.Context) error {
	ticker := time.NewTicker(10 * time.Second)

	for {
		select {
		case <-ticker.C:
			rows, err := pg.Db.Query(ctx, "select raw from mempool")
			if err != nil {
				continue
			}

			txs := make([][]byte, 0)
			tx := make([]byte, 150)

			for rows.Next() {
				if err = rows.Scan(&tx); err != nil {
					break
				}
				txs = append(txs, tx)
			}

			if rows.Err() != nil {
				continue
			}

			rows.Close()

			for _, tx := range txs {
				rq, _ := json.Marshal(req{
					Jsonrpc: "2.0",
					Method:  "sendTransaction",
					Params:  prm{Base64: base64.StdEncoding.EncodeToString(tx)},
					Id:      fmt.Sprintf("%d", time.Now().Nanosecond()),
				})

				url := "https://mainnet.umi.top/json-rpc"
				if global.TestNet {
					url = "https://testnet.umi.top/json-rpc"
				}

				res, err := httpClient.Post(url, "application/json", bytes.NewBuffer(rq))
				if err != nil {
					continue
				}
				_, _ = ioutil.ReadAll(res.Body)
				res.Body.Close()
			}

		case <-ctx.Done():
			ticker.Stop()
			return nil
		}
	}
}

func BlockConfirmer(ctx context.Context) error {
	ticker := time.NewTicker(1 * time.Second)

	for {
		select {
		case <-ticker.C:
			var res *uint64
			for {
				if err := pg.Db.QueryRow(ctx, "select confirm_next_block()").Scan(&res); err != nil {
					break
				}
				if res == nil {
					break
				}
			}
		case <-ctx.Done():
			ticker.Stop()
			return nil
		}
	}
}

func MempoolCleaner(ctx context.Context) error {
	ticker := time.NewTicker(60 * time.Second)

	for {
		select {
		case <-ticker.C:
			if _, err := pg.Db.Exec(ctx, "delete from mempool where created_at + interval '1 day' < now()"); err != nil {
				continue
			}
			if _, err := pg.Db.Exec(ctx, "delete from mempool mm where mm.hash in (select m.hash from mempool m join transaction t on m.hash = t.hash)"); err != nil {
				continue
			}
		case <-ctx.Done():
			ticker.Stop()
			return nil
		}
	}
}
