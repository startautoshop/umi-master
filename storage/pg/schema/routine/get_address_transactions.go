package routine

import (
	"context"
	"github.com/jackc/pgx/v4"
	"umi-master/storage/pg/schema"
)

func init() {
	schema.RegisterRoutine("get_address_transactions", getAddressTransactions)
}

func getAddressTransactions(ver int, tx pgx.Tx) (err error) {
	sql := `
create function get_address_transactions(address_ bytea, limit_ integer default 100)
    returns setof transaction
    language sql
as
$$
	select *
	from transaction
	where sender = address_
       or recipient = address_
       or fee_address = address_
    order by height desc
    limit limit_;
$$;
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	return err
}
