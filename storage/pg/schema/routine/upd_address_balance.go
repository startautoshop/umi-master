package routine

import (
	"context"
	"github.com/jackc/pgx/v4"
	"umi-master/storage/pg/schema"
)

func init() {
	schema.RegisterRoutine("upd_address_balance", updAddressBalance)
}

func updAddressBalance(ver int, tx pgx.Tx) (err error) {
	sql := `
create function upd_address_balance(address bytea,
                                    delta_value bigint,
                                    tx_time timestamptz,
                                    tx_height integer,
                                    comment text default null,
                                    type address_type default null)
    returns void
    language plpgsql
as
$$
declare
    adr_version integer := (get_byte(upd_address_balance.address, 0) << 8) + get_byte(upd_address_balance.address, 1);
    --
    cur_type    address_type;
    cur_value   bigint;
    cur_percent smallint;
    --
    new_value   bigint;
    new_type    address_type;
    new_percent smallint;
    --
    crt_tx      integer;
begin
	select b.confirmed_value, b.confirmed_percent, b.type
	into cur_value, cur_percent, cur_type
	from get_address_balance(upd_address_balance.address, upd_address_balance.tx_time, false) as b;    

	new_type := coalesce(upd_address_balance.type, cur_type); 
	new_value := cur_value + upd_address_balance.delta_value;
	new_percent := cur_percent;

	insert into address_balance_confirmed (address, version, value, percent, type, tx_height, updated_at, created_at, created_tx_height)
	values (upd_address_balance.address, adr_version, new_value, cur_percent, new_type, upd_address_balance.tx_height, upd_address_balance.tx_time, upd_address_balance.tx_time, upd_address_balance.tx_height)
	on conflict on constraint address_balance_confirmed_pkey do update set
		value = new_value,
		percent = cur_percent,
		type = new_type,
		tx_height = upd_address_balance.tx_height,
		updated_at = upd_address_balance.tx_time
	returning created_tx_height into crt_tx;

    insert into address_balance_confirmed_log (address, version, value, percent, type, tx_height, updated_at, delta_value, comment)
    values (upd_address_balance.address, adr_version, new_value, new_percent, new_type, upd_address_balance.tx_height, upd_address_balance.tx_time, upd_address_balance.delta_value, upd_address_balance.comment);

	if crt_tx = upd_address_balance.tx_height
	then
		insert into structure_stats (version, prefix, address_count, updates_at, tx_height)
		values (adr_version, convert_version_to_prefix(adr_version), 1, upd_address_balance.tx_time, upd_address_balance.tx_height)
		on conflict on constraint structure_stats_pk do update
			set address_count = structure_stats.address_count + 1;
	end if;
end
$$;
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	return err
}
