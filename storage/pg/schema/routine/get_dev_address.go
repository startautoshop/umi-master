package routine

import (
	"context"
	"github.com/jackc/pgx/v4"
	"umi-master/global"
	"umi-master/storage/pg/schema"
)

func init() {
	schema.RegisterRoutine("get_dev_address", getDevAddress)
}

func getDevAddress(ver int, tx pgx.Tx) (err error) {
	sql := `
create function get_dev_address()
    returns bytea
    language sql
    immutable
as
$$
	-- umi16uz7khspwq0patw777wgn7hgk6pvds2sxqgwt546z5n489mwmj2szdn2h5
    select decode('55a9d705eb5e01701e1eaddef79c89fae8b682c6c1503010e5d2ba152753976edc95', 'hex');
$$;
`
	if global.TestNet {
		sql = `
create function get_dev_address()
    returns bytea
    language sql
    immutable
as
$$
	-- umi16dhtrj348vaa63lp46u24hs5mjjjxzwqn75qwvnzke6uyr5txukqgckvra
    select decode('55a9d36eb1ca353b3bdd47e1aeb8aade14dca52309c09fa8073262b675c20e8b372c', 'hex');
$$;
`
	}

	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	return err
}
