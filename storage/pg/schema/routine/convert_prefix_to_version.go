package routine

import (
	"context"
	"github.com/jackc/pgx/v4"
	"umi-master/storage/pg/schema"
)

func init() {
	schema.RegisterRoutine("convert_prefix_to_version", convertPrefixToVersion)
}

func convertPrefixToVersion(ver int, tx pgx.Tx) (err error) {
	sql := `
create function convert_prefix_to_version(prefix text)
    returns integer
    language plpgsql
    immutable
as
$$
declare
    --
begin
    if prefix = 'genesis'::text then
        return 0;
    end if;

    return ((get_byte(prefix::bytea, 0) - 96) << 10) +
           ((get_byte(prefix::bytea, 1) - 96) << 5) +
           (get_byte(prefix::bytea, 2) - 96);
end
$$;
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	return err
}
