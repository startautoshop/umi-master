package routine

import (
	"context"
	"github.com/jackc/pgx/v4"
	"umi-master/storage/pg/schema"
)

func init() {
	schema.RegisterRoutine("add_transaction", addTransaction)
}

func addTransaction(ver int, tx pgx.Tx) (err error) {
	sql := `
create function add_transaction(bytes bytea)
    returns void
    language plpgsql
as
$$
begin
    insert into mempool (hash, raw, priority, created_at) values (sha256(bytes), bytes, 0, now());
end
$$;
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	return err
}
