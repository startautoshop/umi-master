package routine

import (
	"context"
	"github.com/jackc/pgx/v4"
	"umi-master/storage/pg/schema"
)

func init() {
	schema.RegisterRoutine("upd_structure_balance", updStructureBalance)
}

func updStructureBalance(ver int, tx pgx.Tx) (err error) {
	sql := `
create function upd_structure_balance(version integer,
                                      delta_value bigint,
                                      epoch timestamptz,
                                      tx_height integer,
                                      comment text default null)
    returns void
    language plpgsql
as
$$
declare
    str_prefix  char(3);
    cur_value   bigint;
    cur_percent smallint;
    new_value   bigint;
    new_percent smallint;
begin
    select value, percent
    into cur_value, cur_percent
    from get_structure_balance(upd_structure_balance.version, upd_structure_balance.epoch);
    --
    new_value := cur_value + upd_structure_balance.delta_value;
    new_percent := cur_percent;
    --
    insert into structure_balance(version, prefix, value, percent, tx_height, updated_at)
    values (upd_structure_balance.version,
            convert_version_to_prefix(upd_structure_balance.version),
            new_value,
            new_percent,
            upd_structure_balance.tx_height,
            upd_structure_balance.epoch)
    on conflict on constraint structure_balance_pk
        do update set
            value      = new_value,
            percent    = new_percent,
            tx_height  = upd_structure_balance.tx_height,
            updated_at = upd_structure_balance.epoch
    returning prefix into str_prefix;
    --
    insert into structure_balance_log (version, prefix, value, percent, tx_height, updated_at, comment)
    values (upd_structure_balance.version, str_prefix, new_value, new_percent, upd_structure_balance.tx_height, upd_structure_balance.epoch, upd_structure_balance.comment);
end
$$;
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	return err
}
