package routine

import (
	"context"
	"github.com/jackc/pgx/v4"
	"umi-master/storage/pg/schema"
)

func init() {
	schema.RegisterRoutine("confirm_tx__genesis", confirmTxGenesis)
}

func confirmTxGenesis(ver int, tx pgx.Tx) (err error) {
	sql := `
create function confirm_tx__genesis(bytes bytea,
                                    tx_height integer,
                                    blk_height integer,
                                    blk_tx_idx integer,
                                    blk_time timestamptz)
    returns void
    language plpgsql
as
$$
declare
    tx_hash   bytea;
    tx_ver    smallint;
    tx_sender bytea;
    tx_recip  bytea;
    tx_value  bigint;
begin
    select hash, version, sender, recipient, value
    into tx_hash, tx_ver, tx_sender, tx_recip, tx_value
    from parse_transaction(bytes);

    perform upd_address_balance(tx_recip, tx_value, blk_time, tx_height, 'genesis', 'umi');

    insert into transaction (hash, height, confirmed_at, block_height, block_tx_idx, version, sender, recipient, value)
    values (tx_hash, tx_height, blk_time, blk_height, blk_tx_idx, tx_ver, tx_sender, tx_recip, tx_value);
end
$$;
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	return err
}
