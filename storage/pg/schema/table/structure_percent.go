package table

import (
	"context"
	"github.com/jackc/pgx/v4"
	"umi-master/storage/pg/schema"
)

func init() {
	schema.RegisterTable("structure_percent", structurePercent)
}

func structurePercent(ver int, tx pgx.Tx) (err error) {
	sql := `
create table structure_percent
(
    version         integer     not null
        constraint structure_percent_pk
            primary key,
    prefix          char(3)     not null,
    level           smallint    not null,
    percent         smallint    not null,
	dev_percent     smallint    not null,
    profit_percent  smallint    not null,
    deposit_percent smallint    not null,
    block_height    integer     not null,
    updated_at      timestamptz not null,
    check (percent between 0 and 4100 and level between 0 and 11)
);
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	return err
}
