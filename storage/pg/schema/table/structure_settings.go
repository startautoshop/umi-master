package table

import (
	"context"
	"github.com/jackc/pgx/v4"
	"umi-master/storage/pg/schema"
)

func init() {
	schema.RegisterTable("structure_settings", structureSettings)
}

func structureSettings(ver int, tx pgx.Tx) (err error) {
	sql := `
create table structure_settings
(
    version        integer                  not null
        constraint structure_settings_pk
            primary key,
    prefix         char(3)                  not null,
    name           text                     not null,
    profit_percent smallint                 not null,
    fee_percent    smallint                 not null,
    dev_address    bytea                    not null,
    master_address bytea                    not null,
    profit_address bytea                    not null,
    fee_address    bytea                    not null,
    created_at     timestamp with time zone not null,
    updated_at     timestamp with time zone not null,
    tx_height      integer                  not null,
    check (profit_percent between 0 and 500 and fee_percent between 0 and 2000)
);
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	sql = `
create unique index structure_settings_prefix_uidx
    on structure_settings (prefix);
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	return err
}
