package view

import (
	"context"
	"github.com/jackc/pgx/v4"
	"umi-master/storage/pg/schema"
)

func init() {
	schema.RegisterView("structure_view", structureView)
}

func structureView(ver int, tx pgx.Tx) (err error) {
	sql := `
create or replace view structure_view as


select prefix,
       name,
       dev_address,
       master_address,
       profit_address,
       fee_address,
       (d).confirmed_value as dev_confirmed,
       (d).composite_value as dev_composite,
       (p).confirmed_value as profit_confirmed,
       (p).composite_value as profit_composite,
       (f).confirmed_value as fee_confirmed,
       (b).value           as balance
from (
         select prefix,
                name,
                encode(dev_address, 'hex')::text    as dev_address,
                encode(master_address, 'hex')::text as master_address,
                encode(profit_address, 'hex')::text as profit_address,
                encode(fee_address, 'hex')::text    as fee_address,
                get_address_balance(dev_address)    as d,
                get_address_balance(profit_address) as p,
                get_address_balance(fee_address)    as f,
                get_structure_balance(version)      as b
         from structure_settings
     ) as s;
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	return err
}
