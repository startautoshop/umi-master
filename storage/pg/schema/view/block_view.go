package view

import (
	"context"
	"github.com/jackc/pgx/v4"
	"umi-master/storage/pg/schema"
)

func init() {
	schema.RegisterView("block_view", blockView)
}

func blockView(ver int, tx pgx.Tx) (err error) {
	sql := `
create view block_view as
select encode(hash, 'hex')::text             as hash,
       height,
       version,
       encode(prev_block_hash, 'hex')::text  as prev_block_hash,
       encode(merkle_root_hash, 'hex')::text as merkle_root_hash,
       created_at,
       tx_count,
       encode(public_key, 'hex')::text       as public_key,
       synced,
       confirmed
from block;
`
	if _, err = tx.Exec(context.Background(), sql); err != nil {
		return err
	}

	return err
}
