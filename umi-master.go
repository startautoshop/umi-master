package main

import (
	"context"
	"encoding/json"
	"flag"
	"log"
	"net/http"
	"umi-master/api/jsonrpc"
	_ "umi-master/api/methods"
	"umi-master/blockchain"
	"umi-master/global"
	"umi-master/storage/pg"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	var tnet = flag.Bool("testnet", false, "Run on the test network instead of the real UMI network")
	var bind = flag.String("bind", "127.0.0.1:8080", "Bind to given address and always listen on it")
	var master = flag.Bool("master", false, "Run as master-node")
	flag.Parse()

	global.TestNet = *tnet
	pg.InitDb()

	fs := http.FileServer(http.Dir("./wallet"))
	http.Handle("/", fs)

	http.HandleFunc("api/json-rpc", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET,POST,OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type")

		if r.Method == http.MethodOptions {
			w.Header().Set("Access-Control-Max-Age", "3600")
		}

		if r.Method != http.MethodPost {
			w.Header().Set("Content-Type", "text/plain; charset=utf-8")
			return
		}

		w.Header().Set("Content-Type", "application/json-rpc")

		var req = &jsonrpc.Request{}
		var res = jsonrpc.NewResponse()

		if err := json.NewDecoder(r.Body).Decode(req); err != nil {
			res.Error = &jsonrpc.ErrorObject{
				Code:    -32700,
				Message: "Parse error",
			}
		} else {
			jsonrpc.CallMethod(req, res)
		}

		if err := json.NewEncoder(w).Encode(res); err != nil {
			log.Println(err)
		}
	})

	ctx := context.Background()
	if *master {
		go blockchain.EventLoop(ctx)
	} else {
		go blockchain.Puller(ctx)
		go blockchain.Pusher(ctx)
	}
	go blockchain.BlockConfirmer(ctx)
	go blockchain.MempoolCleaner(ctx)

	if err := http.ListenAndServe(*bind, nil); err != nil {
		log.Println(err)
	}
}
