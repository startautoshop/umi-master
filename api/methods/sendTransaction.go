package methods

import (
	"context"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"github.com/umi-top/umi-core/transaction"
	"umi-master/api/jsonrpc"
	"umi-master/storage/pg"
)

func init() {
	jsonrpc.RegisterMethod("sendTransaction", sendTransaction)
}

func sendTransaction(req *jsonrpc.Request, res *jsonrpc.Response) {
	params := &struct {
		Base64 string `json:"base64"`
	}{}

	if err := json.Unmarshal(req.Params, params); err != nil {
		res.Error = &jsonrpc.ErrorObject{
			Code:    -32602,
			Message: err.Error(),
		}
		return
	}

	raw, err := base64.StdEncoding.DecodeString(params.Base64)
	if err != nil {
		res.Error = &jsonrpc.ErrorObject{
			Code:    -32603,
			Message: err.Error(),
		}
		return
	}

	if len(raw) != transaction.Length {
		res.Error = &jsonrpc.ErrorObject{
			Code:    -32602,
			Message: fmt.Sprintf("transaction size should be %d bytes", transaction.Length),
		}
		return
	}

	tx := transaction.FromBytes(raw)
	if err = tx.Verify(); err != nil {
		res.Error = &jsonrpc.ErrorObject{
			Code:    -32602,
			Message: err.Error(),
		}
		return
	}

	var ok bool
	if err := pg.Db.QueryRow(context.Background(), "select exists (select 1 from transaction where hash = $1)", tx.Hash()).Scan(&ok); err != nil {
		res.Error = &jsonrpc.ErrorObject{
			Code:    -32603,
			Message: err.Error(),
		}
		return
	}

	if !ok {
		sql := "insert into mempool (hash, raw, priority) values ($1,$2,$3) on conflict do nothing"
		if _, err = pg.Db.Exec(context.Background(), sql, tx.Hash(), tx.ToBytes(), tx.Version()); err != nil {
			res.Error = &jsonrpc.ErrorObject{
				Code:    -32603,
				Message: err.Error(),
			}
			return
		}
	}

	res.Result = &struct {
		Hash string `json:"hash"`
	}{
		Hash: hex.EncodeToString(tx.Hash()),
	}
}
