package methods

import (
	"context"
	"encoding/hex"
	"encoding/json"
	"github.com/btcsuite/btcutil/bech32"
	"github.com/umi-top/umi-core/address"
	"time"
	"umi-master/api/jsonrpc"
	"umi-master/storage/pg"
)

func init() {
	jsonrpc.RegisterMethod("listTransactions", listTransactions)
}

func listTransactions(req *jsonrpc.Request, res *jsonrpc.Response) {
	params := &struct {
		Address string `json:"address"`
	}{}

	if err := json.Unmarshal(req.Params, params); err != nil {
		res.Error = &jsonrpc.ErrorObject{
			Code:    -32602,
			Message: err.Error(),
		}
		return
	}

	if _, _, err := bech32.Decode(params.Address); err != nil {
		res.Error = &jsonrpc.ErrorObject{
			Code:    -32602,
			Message: "Invalid params",
		}
		return
	}

	adr := address.FromBech32(params.Address).ToBytes()

	sql := "select hash, height, confirmed_at, block_height, block_tx_idx, version, sender, recipient, value, fee_address, fee_value, struct from get_address_transactions($1::bytea,$2)"
	rows, err := pg.Db.Query(context.Background(), sql, adr, int32(100))

	if err != nil {
		res.Error = &jsonrpc.ErrorObject{
			Code:    -32603,
			Message: err.Error(),
		}
		return
	}

	defer rows.Close()

	type Struct struct {
		Prefix *string `json:"prefix,omitempty"`
	}

	type Tx struct {
		Hash        string  `json:"hash"`
		Height      int32   `json:"height"`
		ConfirmedAt int64   `json:"confirmed_at"`
		BlockHeight int32   `json:"block_height"`
		BlockTxIdx  int32   `json:"block_tx_idx"`
		Version     int16   `json:"version"`
		Sender      string  `json:"sender"`
		Recipient   *string `json:"recipient,omitempty"`
		Value       *int64  `json:"value,omitempty"`
		FeeAddress  *string `json:"fee_address,omitempty"`
		FeeValue    *int64  `json:"fee_value,omitempty"`
		Structure   *Struct `json:"structure,omitempty"`
	}

	txs := make([]Tx, 0, rows.CommandTag().RowsAffected())

	var hash []byte
	var sender []byte
	var recipt []byte
	var confAt time.Time
	var feeAdr []byte
	var struc *Struct

	for rows.Next() {
		tx := Tx{}

		if err = rows.Scan(&hash, &tx.Height, &confAt, &tx.BlockHeight, &tx.BlockTxIdx, &tx.Version, &sender, &recipt, &tx.Value, &feeAdr, &tx.FeeValue, &struc); err != nil {
			res.Error = &jsonrpc.ErrorObject{
				Code:    -32603,
				Message: err.Error(),
			}
			return
		}

		tx.Hash = hex.EncodeToString(hash)
		tx.ConfirmedAt = confAt.Unix()

		if struc != nil {
			tx.Structure = struc
		}

		tx.Sender = address.FromBytes(sender).ToBech32()

		if len(recipt) > 0 {
			rec := address.FromBytes(recipt).ToBech32()
			tx.Recipient = &rec
		}

		if len(feeAdr) > 0 {
			feeAdrStr := address.FromBytes(feeAdr).ToBech32()
			tx.FeeAddress = &feeAdrStr
		}

		txs = append(txs, tx)
	}

	res.Result = txs
}
